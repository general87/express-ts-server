# TypeScript ExpressJS Server


Clone project

```
cd project
yarn or npm install
```


Create environment variables from the .env file.
```
PORT=8000
```

Start server
```
yarn run start
```

Run test
```
yarn run test
```

Start dev 
```
yarn run dev
```

Run build
```
yarn run build
```




